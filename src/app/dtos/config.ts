export interface ChannelConfig {
  JoinedChannels: Array<string>;
}

export interface ConnectionConfig {
  URL: string;
  Username: string;
  Token: string;
  ChannelConfig: ChannelConfig;
}

export enum MessageRateModes {
  USER = 'USER', KNOWN = 'KNOWN', VERIFIED = 'VERIFIED'
}

export interface MessagingConfig {
  MessageRateMode: MessageRateModes;
}

export enum CooldownMode {
  PerUser = 'PER_USER', AllUsers = 'ALL_USERS'
}

export enum PermissionsMode {
  StreamerOnly = 'STREAMER', ModsOnly = 'MODS', SubscribersOnly = 'SUBSCRIBER', Selected = 'SELECTED'
}

export interface PermConfigObject {
  [key: string]: PermissionsConfig;
}

export interface PermissionsConfig {
  Mode: PermissionsMode;
  Selected: Array<string>;
}

export interface CooldownConfig {
  Amount: number;
  Mode: CooldownMode;
}

export interface CommandConfig {
  GlobalCooldown: number;
  Cooldowns: Map<string, CooldownConfig>;
  Permissions: PermConfigObject;
}

export interface Authentication {
  Token: string;
}

export interface AuthConfig {
  Authentication: Authentication;
}

export interface StaticFilesConfig {
  RootFolderPath: string;
}

export interface Config {
  ConnectionConfig: ConnectionConfig;
  MessagingConfig: MessagingConfig;
  CommandConfig: CommandConfig;
  AuthConfig: AuthConfig;
  StaticFilesConfig: StaticFilesConfig;
}

export function isConfig(o: any): o is Config {
  return o.ConnectionConfig !== undefined && o.MessagingConfig !== undefined && o.CommandConfig !== undefined;
}
