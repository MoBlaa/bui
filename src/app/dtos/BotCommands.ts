export enum EventID {
  GetConfig = 'GETCONFIG',
  SetConfig = 'SETCONFIG',
  Start = 'START',
  Stop = 'STOP',
  ConnectionSuccess = 'CONNECTED',
  CommandStarted = 'STARTED COMMAND',
  CommandFinished = 'FINISHED COMMAND',
  CommandNotSupported = 'FINISHED COMMAND',
  CommandCooldown = 'COMMAND COOLDOWN',
  NotPermitted = 'NOT PERMITTED',
  BotStarted = 'STARTED BOT',
  BotStopped = 'STOPPED BOT',
  BotFinalizing = 'FINALIZING BOT',

  // Log level ids
  Debug = 'DEBUG',
  Info = 'INFO',
  Error = 'ERROR',
  Warning = 'WARNING',

  // Chat messages
  ChatMessage = 'CHAT MESSAGE'
}

export enum EventType {
  LogEvent = 'LOG EVENT',
  CommandResponse = 'COMMAND RESPONSE',
  Command = 'COMMAND'
}

export interface BotEvent {
  Type: EventType;
  ID: EventID;
  Body?: any;
}

export function isBotEvent(o: any): o is BotEvent {
  return o.Type !== undefined && o.ID !== undefined;
}

export interface CommandResponse extends BotEvent {
  Code: number;
}

export function isCommandResponse(o: any): o is CommandResponse {
  return o.Code !== undefined && o.Type === EventType.CommandResponse && isBotEvent(o);
}

export enum LogLevel {
  Debug = 'DEBUG', Info = 'INFO', Error = 'ERROR', Warning = 'WARNING'
}

export interface LogEvent extends BotEvent {
  Level: LogLevel;
}

export function isLogEvent(o: any): o is LogEvent {
  return o.Type === EventType.LogEvent && o.Level !== undefined && isBotEvent(o);
}
