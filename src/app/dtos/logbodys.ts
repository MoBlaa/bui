import {isArray} from 'util';

export interface ChatMessage {
  Tags: object;
  Command: string;
  Params: Array<string>;
  Message: string;
}

export function isChatMessage(o: any): o is ChatMessage {
  return o != null && o.Tags !== undefined && typeof o.Tags === 'object' &&
    o.Command !== undefined && typeof o.Command === 'string' &&
    o.Params !== undefined && isArray(o.Params) &&
    o.Message !== undefined && typeof o.Message === 'string';
}
