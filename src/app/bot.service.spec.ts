import {TestBed} from '@angular/core/testing';

import {BotService} from './bot.service';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {BotStatus, State} from './reducers/daos';
import {Store} from '@ngrx/store';

describe('BotService', () => {
  let store: MockStore<State>;
  const initialState: State = {
    authtoken: 'xxxx-xxxx-xxxx',
    status: BotStatus.Unknown
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({initialState})
      ],
    });
  });
  beforeEach(() => {
    store = TestBed.get<Store<State>>(Store);
  });

  it('should be created', () => {
    const service: BotService = TestBed.get(BotService);
    expect(service).toBeTruthy();
  });
});
