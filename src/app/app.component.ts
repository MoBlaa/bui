import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {BehaviorSubject} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {NavigationEnd, Router} from '@angular/router';
import {BotStatus, State} from './reducers/daos';
import {BotService} from './bot.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // To use the enum in the template
  botStatus = BotStatus;

  // TODO: Change title with env var or other from backend
  title = 'Bot';
  connected = new BehaviorSubject<boolean>(false);

  status: BotStatus;

  constructor(
    private store: Store<State>,
    private router: Router,
    private cfRef: ChangeDetectorRef,
    private service: BotService
  ) {
  }

  ngOnInit(): void {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .pipe(map(event => event as NavigationEnd))
      .pipe(map(event => event.url.replace('/', '').trim()))
      .pipe(filter(paths => paths.length >= 2))
      .pipe(map(url => url[0].toUpperCase() + url.slice(1)))
      .subscribe(event => {
        this.title = `Bot - ${event}`;
      });
    this.store.subscribe(state => {
      this.connected.next(state.authtoken !== '');
      console.log(`Current bot status: ${state.status}`);
      this.status = state.status;
      this.cfRef.detectChanges();
    });
  }

  toggleState() {
    // Starts the bot if not started
    // Stops the Bot if not stopped
    if (this.status === BotStatus.Running) {
      this.service.stop();
    } else {
      this.service.start();
    }
  }
}
