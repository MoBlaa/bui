import {createAction, props} from '@ngrx/store';

export const set = createAction(
  '[Authentication] SetCode',
  props<{ token: string }>(),
);
export const reset = createAction('[Authentication] Reset');
