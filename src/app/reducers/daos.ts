export interface State {
  authtoken: string;
  status: BotStatus;
}

export enum BotStatus {
  Running = 'RUNNING', Stopped = 'STOPPED', Unknown = 'UNKNOWN'
}
