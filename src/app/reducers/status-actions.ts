import {createAction} from '@ngrx/store';

export const stop = createAction('[Status] Stop');
export const start = createAction('[Status] Start');
export const unknown = createAction('[Status] Unknown');
