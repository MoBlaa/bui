import {ActionReducer, ActionReducerMap, MetaReducer, } from '@ngrx/store';
import {environment} from '../../environments/environment';
import {authtokenReducer, statusReducer} from './reducers';
import {State} from './daos';

export const reducers: ActionReducerMap<State> = {
  authtoken: authtokenReducer,
  status: statusReducer
};

// Debug function for non-production prints actions to console
export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    console.log(`state: ${JSON.stringify(state, null, 1)}`);
    console.log(`action: ${JSON.stringify(action, null, 1)}`);
    return reducer(state, action);
  };
}

const localStorageKey = 'SaveSate';

interface LocalStorageEntry {
  authtoken: string;
}

function isLocalStorageEntry(o: any): boolean {
  return o != null && o.authtoken !== undefined;
}

function getSavedState(): LocalStorageEntry {
  const parsed = JSON.parse(localStorage.getItem(localStorageKey));
  if (isLocalStorageEntry(parsed)) {
    return parsed;
  } else {
    return {authtoken: ''};
  }
}

function setSavedState(state: LocalStorageEntry) {
  localStorage.setItem(localStorageKey, JSON.stringify(state));
}

function merge(base: any, additions: any): any {
  const tmp = {...base};
  for (const attr in tmp) {
    if (typeof additions[attr] !== undefined && typeof additions[attr] === typeof tmp[attr]) {
      tmp[attr] = additions[attr];
    }
  }
  return tmp;
}

export function storageStateReducer(reducer: ActionReducer<State>): ActionReducer<State> {
  let onInit = true;
  return (state, action) => {
    const nextState = reducer(state, action);
    if (onInit) {
      onInit = false;
      const saved = getSavedState();
      if (saved != null) {
        return merge(nextState, saved);
      }
    }

    // Convert to LocalStorageEntry
    const entry: LocalStorageEntry = {
      authtoken: nextState.authtoken
    };

    setSavedState(entry);
    return nextState;
  };
}


export const metaReducers: MetaReducer<State>[] = !environment.production ? [debug, storageStateReducer] : [storageStateReducer];
