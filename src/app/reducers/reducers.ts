import {BotStatus, State} from './daos';
import {Action, createReducer, on} from '@ngrx/store';
import {reset, set} from './auth-actions';
import {start, stop, unknown} from './status-actions';

const initialState: State = {
  authtoken: '',
  status: BotStatus.Unknown,
};

const pAuthReducer = createReducer(initialState.authtoken,
  on(set, (_, action) => action.token),
  on(reset, () => '')
);

export function authtokenReducer(state: string, action: Action) {
  return pAuthReducer(state, action);
}

const pStatusReducer = createReducer(initialState.status,
  on(stop, () => BotStatus.Stopped),
  on(start, () => BotStatus.Running),
  on(unknown, () => BotStatus.Unknown)
);

export function statusReducer(state: BotStatus, action: Action) {
  return pStatusReducer(state, action);
}
