import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LogoutComponent} from './logout.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {BotStatus, State} from '../reducers/daos';
import {Store} from '@ngrx/store';
import {BotService} from '../bot.service';

class MockBotService {
}

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let store: MockStore<State>;
  let botService: BotService;
  const initialState: State = {
    authtoken: '',
    status: BotStatus.Unknown
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({initialState}),
        { provide: BotService, useClass: MockBotService }
      ],
      declarations: [LogoutComponent]
    })
      .compileComponents();
  });
  beforeEach(() => {
    store = TestBed.get<Store<State>>(Store);
    botService = TestBed.get<BotService>(BotService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
