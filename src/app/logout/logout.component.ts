import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../reducers/daos';
import {reset} from '../reducers/auth-actions';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private store: Store<State>) {
  }

  ngOnInit() {
    this.store.dispatch(reset());
  }

}
