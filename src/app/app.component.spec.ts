import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {MatTooltipModule} from '@angular/material';
import {AppModule} from './app.module';
import {SettingsModule} from './settings/settings.module';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {HttpClientModule} from '@angular/common/http';
import {LogModule} from './log/log.module';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {PageNotFoundComponent} from './pagenotfound/page-not-found.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {BotStatus, State} from './reducers/daos';
import {Store} from '@ngrx/store';

describe('AppComponent', () => {
  let store: MockStore<State>;
  const initialState: State = {
    authtoken: '',
    status: BotStatus.Unknown
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SettingsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatSidenavModule,
        MatIconModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule,
        MatListModule,
        MatDividerModule,
        FlexLayoutModule,
        MatToolbarModule,
        MatTooltipModule,
        HttpClientModule,
        LogModule
      ],
      declarations: [
        AppComponent,
        LoginComponent,
        LogoutComponent,
        PageNotFoundComponent
      ],
      providers: [
        provideMockStore({initialState})
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    store = TestBed.get<Store<State>>(Store);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'bui'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Bot');
  });
});
