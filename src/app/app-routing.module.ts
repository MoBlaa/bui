import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './logout/logout.component';
import {SettingsComponent} from './settings/settings.component';
import {LogComponent} from './log/log.component';
import {PageNotFoundComponent} from './pagenotfound/page-not-found.component';


const routes: Routes = [
  {path: 'logout', component: LogoutComponent},
  {path: 'settings', component: SettingsComponent},
  {path: 'log', component: LogComponent},
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
