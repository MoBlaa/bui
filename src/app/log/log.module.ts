import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogComponent} from './log.component';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import {LogEntryComponent} from './logentry/log-entry.component';


@NgModule({
  declarations: [
    LogComponent,
    LogEntryComponent,
  ],
  imports: [
    CommonModule,
    MatListModule,
    MatDividerModule,
    FlexLayoutModule,
    MatTableModule,
    MatSelectModule,
    MatPaginatorModule
  ]
})
export class LogModule {
}
