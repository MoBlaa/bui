import {Component, Input, OnInit} from '@angular/core';
import {EventID, LogEvent} from 'src/app/dtos/BotCommands';
import {isString} from 'util';
import {isChatMessage} from 'src/app/dtos/logbodys';

interface LogMessageDao {
  Class?: string;
  Highlight?: string;
  Message: string;
}

@Component({
  selector: 'app-logentry',
  templateUrl: './log-entry.component.html',
  styleUrls: ['./log-entry.component.scss']
})
export class LogEntryComponent implements OnInit {

  @Input() event: LogEvent;
  mssg: LogMessageDao;

  constructor() {
  }

  ngOnInit() {
    this.mssg = this.toLogMessageDao(this.event);
  }

  toLogMessageDao(message: LogEvent): LogMessageDao {
    if (message === undefined) {
      return null;
    }
    const body = message.Body;
    let response: LogMessageDao = {Class: message.Level, Message: ''};
    if (message.ID === EventID.ChatMessage && isChatMessage(body)) {
      response.Highlight = body.Command;
      switch (body.Command) {
        case 'PING':
          response.Message = body.Message;
          break;
        case 'ROOMSTATE':
        case 'USERSTATE':
          response.Message = JSON.stringify(body.Tags);
          break;
        case 'HOSTTARGET':
        case 'NOTICE':
        case 'PRIVMSG':
          if (body.Tags != null && body.Tags['user-id'] !== undefined) {
            response = {Highlight: `${body.Tags['display-name']}`, Message: body.Message};
          } else {
            response.Message = `${body.Message}`;
          }
          break;
        default:
          response.Message = JSON.stringify(body);
      }
    } else if (isString(body)) {
      response = {Message: body};
    } else {
      response = {Message: JSON.stringify(body)};
    }
    return response;
  }
}
