import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LogComponent} from './log.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {BotStatus, State} from '../reducers/daos';
import {Store} from '@ngrx/store';
import {LogEntryComponent} from './logentry/log-entry.component';
import {CommonModule} from '@angular/common';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {BotService} from '../bot.service';
import {Observable} from 'rxjs';

describe('LogComponent', () => {
  let component: LogComponent;
  let fixture: ComponentFixture<LogComponent>;
  let store: MockStore<State>;
  let service: jasmine.SpyObj<BotService>;
  const initialState: State = {
    authtoken: '',
    status: BotStatus.Unknown
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({initialState}),
        {provide: BotService, useValue: jasmine.createSpyObj('BotService', ['getLog'])}
      ],
      declarations: [
        LogComponent,
        LogEntryComponent,
      ],
      imports: [
        CommonModule,
        MatListModule,
        MatDividerModule,
        FlexLayoutModule,
        MatTableModule,
        MatSelectModule,
        MatPaginatorModule,
        NoopAnimationsModule
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    store = TestBed.get<Store<State>>(Store);
    service = TestBed.get<BotService>(BotService);
    service.getLog.and.returnValue(new Observable());
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
