import {Component, OnInit, ViewChild} from '@angular/core';
import {BotService} from '../bot.service';
import {LogEvent, LogLevel} from '../dtos/BotCommands';
import {MatTableDataSource} from '@angular/material/table';
import {ChatMessage} from '../dtos/logbodys';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {

  logLevels = [LogLevel.Info, LogLevel.Debug, LogLevel.Warning, LogLevel.Error];

  messages: Array<LogEvent> = [];
  dataSource = new MatTableDataSource(this.messages);
  displayedColumns = ['Body'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private botService: BotService,
  ) {
  }

  ngOnInit() {
    this.botService.getLog().subscribe(
      next => {
        this.messages.unshift(next);
        this.dataSource = new MatTableDataSource(this.messages);
        this.dataSource.paginator = this.paginator;
      }, console.error,
      () => console.log('COMPLETE!')
    );
  }

  toString(message: ChatMessage): string {
    switch (message.Command) {
      case 'PING':
        return `${message.Command} ${message.Message}`;
      case 'ROOMSTATE':
      case 'USERSTATE':
        return `${message.Command} ${JSON.stringify(message.Tags)}`;
      case 'HOSTTARGET':
      case 'NOTICE':
      case 'PRIVMSG':
        if (message.Tags != null && message.Tags['user-id'] !== undefined) {
          return `${message.Tags['user-id']}: ${message.Message}`;
        } else {
          return `${message.Command} ${message.Message}`;
        }
      default:
        return JSON.stringify(message);
    }
  }

  start() {
    this.botService.start();
  }

  stop() {
    this.botService.stop();
  }
}
