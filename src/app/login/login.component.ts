import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {State} from '../reducers/daos';
import {set} from '../reducers/auth-actions';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  code: string;

  constructor(
    private router: Router,
    private store: Store<State>,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.queryParams
      .pipe(filter(m => m.token !== undefined))
      .pipe(map(m => m.token))
      .pipe(filter(token => token != null && token !== ''))
      .subscribe(token => this.code = token);
    this.store.pipe(map(state => state.authtoken))
      .pipe(filter(token => token != null && token !== ''))
      .subscribe(token => this.code = token);
  }

  connect(code: string) {
    this.store.dispatch(set({token: code}));
    this.router.navigate(['/settings'])
      .catch(console.error);
  }
}
