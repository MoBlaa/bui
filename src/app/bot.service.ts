import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable, ReplaySubject} from 'rxjs';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {BotEvent, EventID, EventType, isCommandResponse, isLogEvent, LogEvent} from './dtos/BotCommands';
import {Config} from './dtos/config';
import {State} from './reducers/daos';
import {filter, map} from 'rxjs/operators';
import {start, stop} from './reducers/status-actions';

export interface BotResponse {
  Code: number;
  Body: any;
}

const baseUrl = 'ws://localhost:8080/ws';

@Injectable({
  providedIn: 'root'
})
export class BotService {
  socket: WebSocketSubject<BotEvent>;
  token: string;
  cfg: Config;

  log: ReplaySubject<BotEvent>;

  constructor(
    store: Store<State>
  ) {
    this.log = new ReplaySubject(100);
    store.pipe(map(state => state.authtoken))
      .pipe(filter(token => token != null && token !== '' && token !== this.token))
      .subscribe(token => {
        // Check if socket not yet closed
        if (this.socket != null) {
          this.socket.complete();
          this.socket = null;
        }
        // Set token to allow check for change
        this.token = token;

        const socket: WebSocketSubject<BotEvent> = webSocket(`${baseUrl}?token=${token}`);

        socket.subscribe(
          (msg: BotEvent) => {
            console.log('Received message: ' + JSON.stringify(msg, null, 2));
            if (msg.Type === EventType.LogEvent) {
              this.log.next(msg);
            } else if (isCommandResponse(msg)) {
              if (msg.ID === EventID.Start && msg.Code === 200) {
                store.next(start());
              } else if (msg.ID === EventID.Stop && msg.Code === 200) {
                store.next(stop());
              } else if (msg.ID === EventID.Start && msg.Code === 400) {
                store.next(start());
              }
            }
          },
          console.error,
          () => console.log('Closed connection!')
        );
        this.socket = socket;
      });
  }

  private send(data: BotEvent) {
    console.log(`Sending data: ${JSON.stringify(data, null, 2)}`);
    this.socket.next(data);
  }

  start() {
    this.send({
      ID: EventID.Start,
      Type: EventType.Command
    });
  }

  stop() {
    this.send({
      ID: EventID.Stop,
      Type: EventType.Command
    });
  }

  getConfig(): Observable<Config> {
    this.send({
      ID: EventID.GetConfig,
      Type: EventType.Command
    });

    return this.socket
      .pipe(filter(event => event.ID === EventID.GetConfig))
      .pipe(map(event => (event.Body) as Config));
  }

  setConfig(cfg: Config) {
    this.send({
      ID: EventID.SetConfig,
      Type: EventType.Command,
      Body: cfg
    });
  }

  getLog(): Observable<LogEvent> {
    return this.log.pipe(filter(isLogEvent));
  }
}
