import {Component, OnInit} from '@angular/core';
import {BotService} from '../bot.service';
import {Config, MessageRateModes, PermissionsMode} from '../dtos/config';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';

export const urlPattern = /^(?:ws(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  config: Config = {
    CommandConfig: null,
    MessagingConfig: null,
    ConnectionConfig: null,
    AuthConfig: null,
    StaticFilesConfig: null
  };
  hideAuthToken = true;
  permissionModes = [PermissionsMode.StreamerOnly, PermissionsMode.ModsOnly, PermissionsMode.SubscribersOnly, PermissionsMode.Selected];
  perms = PermissionsMode;
  messageRateModes = [MessageRateModes.USER, MessageRateModes.KNOWN, MessageRateModes.VERIFIED];
  separatorKeysCodes = [ENTER, COMMA];

  saveEnabled = true;

  configForm: FormGroup;

  set formConfig(cfg: Config) {
    this.configForm.setValue({
      ConnectionConfig: {
        URL: cfg.ConnectionConfig.URL,
        Username: cfg.ConnectionConfig.Username,
        Token: cfg.ConnectionConfig.Token,
        Channels: cfg.ConnectionConfig.ChannelConfig.JoinedChannels,
        MessageRate: cfg.MessagingConfig.MessageRateMode
      },
      CommandConfig: {
        GlobalCooldown: cfg.CommandConfig.GlobalCooldown,
        Permissions: cfg.CommandConfig.Permissions
      }
    });
  }

  get formConfig(): Config {
    const value = this.configForm.value;
    return {
      ConnectionConfig: {
        URL: value.ConnectionConfig.URL,
        Username: value.ConnectionConfig.Username,
        Token: value.ConnectionConfig.Token,
        ChannelConfig: {
          JoinedChannels: value.ConnectionConfig.Channels,
        }
      },
      MessagingConfig: {
        MessageRateMode: value.ConnectionConfig.MessageRate
      },
      CommandConfig: {
        GlobalCooldown: value.CommandConfig.GlobalCooldown,
        Permissions: value.CommandConfig.Permissions,
        Cooldowns: this.config.CommandConfig.Cooldowns
      },
      AuthConfig: {
        Authentication: {
          Token: this.config.AuthConfig.Authentication.Token
        }
      },
      StaticFilesConfig: {
        RootFolderPath: this.config.StaticFilesConfig.RootFolderPath
      }
    };
  }

  getListenedChannels(): Array<string> {
    return this.formConfig.ConnectionConfig.ChannelConfig.JoinedChannels;
  }

  // TODO: make permissions-selected like channels (currently doesn't save), Add Cooldowns

  constructor(
    private botService: BotService,
  ) {
    this.configForm = new FormGroup({
      ConnectionConfig: new FormGroup({
        URL: new FormControl('', [Validators.required, Validators.pattern(urlPattern)]),
        Username: new FormControl('', [Validators.required]),
        Token: new FormControl('', [Validators.required, Validators.pattern(/^oauth:[\w\d]+$/)]),
        Channels: new FormControl([], [Validators.required, Validators.pattern(/^(\w{4,25})(,\w{4,25}){0,}$/)]),
        MessageRate: new FormControl(MessageRateModes.USER, [Validators.required])
      }),
      CommandConfig: new FormGroup({
        GlobalCooldown: new FormControl('0', [Validators.required, Validators.pattern(/^\d+$/)]),
        Permissions: new FormControl({})
      })
    });
  }

  ngOnInit() {
    this.botService.getConfig().subscribe(cfg => {
      this.config = cfg;
      this.formConfig = cfg;
    });
    this.configForm.statusChanges.subscribe(state => this.saveEnabled = state === 'VALID');
    this.configForm.statusChanges.subscribe(state => console.log(`State: ${state}, typeof State: ${typeof state}`));
  }

  globalCooldownError(): string {
    return this.configForm.get('CommandConfig').get('GlobalCooldown').hasError('required') ? 'You must enter a value' :
      this.configForm.get('CommandConfig').get('GlobalCooldown').hasError('pattern') ? 'You must enter a number' : '';
  }

  getConnectionUrlError(): string {
    return this.configForm.get('ConnectionConfig').get('URL').hasError('required') ? 'You must enter a value' :
      this.configForm.get('ConnectionConfig').get('URL').hasError('pattern') ? 'You must enter a valid url' : '';
  }

  getConnectionUsernameError(): string {
    return this.configForm.get('ConnectionConfig').get('Username').hasError('required') ? 'You must enter a value' : '';
  }

  getConnectionTokenError(): string {
    return this.configForm.get('ConnectionConfig').get('Token').hasError('required') ? 'You must enter a value' :
      this.configForm.get('ConnectionConfig').get('Token').hasError('pattern') ? 'The Token has to have the format "oauth:..."' : '';
  }

  getConnectionChannelsError(): string {
    return this.configForm.get('ConnectionConfig').get('Channels').hasError('required') ?
      'You must enter a value' :
      this.configForm.get('ConnectionConfig').get('Channels').hasError('pattern') ?
        'The Channels have to be a comma separated list (no spaces) of usernames (consisting of a-z, A-Z, 0-9 and underscores "_")' : '';
  }

  getConnectionMessageRateError(): string {
    return this.configForm.get('ConnectionConfig').get('MessageRate').hasError('required') ? 'You must enter a value' : '';
  }

  addChannel(event: MatChipInputEvent) {
    this.configForm.get('ConnectionConfig').get('Channels').value.push(event.value.trim());
    event.input.value = '';
  }

  removeChannel(channel: string) {
    const channels = this.configForm.get('ConnectionConfig').get('Channels').value;
    const index = channels.indexOf(channel);
    channels.splice(index, 1);
  }

  reset() {
    this.formConfig = this.config;
  }

  submit() {
    if (this.configForm.invalid) {
      // Show error snackbar
      console.log('Form has to be valid to set the configuration');
      return;
    }

    // Build config object from form

    this.botService.setConfig(this.formConfig);
    console.log('Submitting changes!');
  }
}
