import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsComponent} from './settings.component';
import {ArrayFormFieldComponent} from './array-form-field/array-form-field.component';
import {PermissionsComponent} from './permissions/permissions.component';
import {CommonModule} from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {BotStatus, State} from '../reducers/daos';
import {Store} from '@ngrx/store';
import {BotService} from '../bot.service';
import {Observable} from 'rxjs';
import {Config} from '../dtos/config';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;
  let store: MockStore<State>;
  let service: jasmine.SpyObj<BotService>;
  const initialState: State = {
    authtoken: '',
    status: BotStatus.Unknown
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({initialState}),
        {provide: BotService, useValue: jasmine.createSpyObj('BotService', ['getConfig', 'setConfig'])}
      ],
      declarations: [
        SettingsComponent,
        ArrayFormFieldComponent,
        PermissionsComponent,
      ],
      imports: [
        NoopAnimationsModule,
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MatIconModule,
        MatInputModule,
        MatExpansionModule,
        MatSelectModule,
        MatChipsModule,
        MatListModule,
        MatButtonModule
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    store = TestBed.get<Store<State>>(Store);
    service = TestBed.get<BotService>(BotService);
    service.getConfig.and.returnValue(new Observable());
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
