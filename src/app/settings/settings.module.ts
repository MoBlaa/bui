import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsComponent} from './settings.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {ArrayFormFieldComponent} from './array-form-field/array-form-field.component';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PermissionsComponent} from './permissions/permissions.component';


@NgModule({
  declarations: [
    SettingsComponent,
    ArrayFormFieldComponent,
    PermissionsComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatIconModule,
    MatInputModule,
    MatExpansionModule,
    MatSelectModule,
    MatChipsModule,
    MatListModule,
    MatButtonModule
  ]
})
export class SettingsModule {
}
