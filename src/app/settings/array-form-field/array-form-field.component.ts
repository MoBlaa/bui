import {Component, forwardRef, Input} from '@angular/core';
import {MatChipInputEvent} from '@angular/material/chips';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {COMMA, ENTER, SPACE} from '@angular/cdk/keycodes';

@Component({
  selector: 'app-array-form-field',
  templateUrl: './array-form-field.component.html',
  styleUrls: ['./array-form-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ArrayFormFieldComponent),
      multi: true
    }
  ]
})
export class ArrayFormFieldComponent implements ControlValueAccessor {

  @Input() class = '';
  @Input() hint = '';
  @Input() label = '';
  @Input() values: Array<string> = [];
  @Input() appearance = 'standard';

  disabled = false;

  separatorKeysCodes = [COMMA, ENTER, SPACE];

  propagateChange = (_: any) => {};

  constructor() {
  }

  add(event: MatChipInputEvent) {
    event.input.value = '';
    this.values.push(event.value);
    this.propagateChange(this.values);
  }

  remove(value: string) {
    const index = this.values.indexOf(value);
    this.values.splice(index, 1);
    this.propagateChange(this.values);
  }

  getError(): string {
    return '';
  }

  writeValue(obj: any): void {
    if (obj !== undefined) {
      this.values = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
