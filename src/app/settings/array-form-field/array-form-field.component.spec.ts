import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ArrayFormFieldComponent} from './array-form-field.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('ArrayFormFieldComponent', () => {
  let component: ArrayFormFieldComponent;
  let fixture: ComponentFixture<ArrayFormFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArrayFormFieldComponent],
      imports: [
        NoopAnimationsModule,
        MatIconModule,
        MatChipsModule,
        MatFormFieldModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrayFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
