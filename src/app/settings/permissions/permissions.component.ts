import {Component, forwardRef, Input} from '@angular/core';
import {PermConfigObject, PermissionsMode} from 'src/app/dtos/config';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatSelectChange} from '@angular/material/select';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PermissionsComponent),
      multi: true
    }
  ]
})
export class PermissionsComponent implements ControlValueAccessor {

  permissionModes = [PermissionsMode.StreamerOnly, PermissionsMode.ModsOnly, PermissionsMode.SubscribersOnly, PermissionsMode.Selected];

  @Input() value: PermConfigObject = {};

  propagateChange = (_: any) => {};

  constructor() {
  }

  writeValue(obj: any): void {
    if (obj !== undefined) {
      for (const attr in obj) {
        if (obj.hasOwnProperty(attr) && obj[attr].Selected == null) {
          obj[attr].Selected = [];
        }
      }
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }

  hasSelectedMode(cmd: string): boolean {
    return this.value[cmd].Mode === PermissionsMode.Selected;
  }

  changePermission(key: string, event: MatSelectChange) {
    console.log(`Changed permission '${key}' to: ${event.value}`);
    this.value[key].Mode = event.value;
  }

}
